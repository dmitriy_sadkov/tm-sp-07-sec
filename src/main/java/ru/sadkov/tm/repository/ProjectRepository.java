package ru.sadkov.tm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.enumerate.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface ProjectRepository extends CrudRepository<Project, String> {
    List<Project> findAll();

    Project findOneByName(String projectName);

    boolean existsByName(String projectName);

    void deleteByName(String projectName);

    @Modifying
    @Query("update Project set description = :description, name = :name where id = :id")
    void update(@Param("id") String projectId, @Param("description") String description, @Param("name") String newName);

    @Query("select p from Project p where  p.name like %:part% or p.description like %:part%")
    List<Project> findProjectsByPart(@Param("part") String part);

    @Query("select p from Project p where p.status = :status")
    List<Project> findProjectsByStatus(@Param("status") Status status);

    @Modifying
    @Query("update Project set status = :status, dateBegin = :date where name = :name")
    void startProject(@Param("status") Status process, @Param("name") String projectName, @Param("date") Date startDate);

    @Modifying
    @Query("update Project set status = :status, dateEnd = :date where name = :name")
    void endProject(@Param("status") Status done, @Param("name") String projectName, @Param("date") Date endDate);

    List<Project> findAllByUserId(String id);
}
