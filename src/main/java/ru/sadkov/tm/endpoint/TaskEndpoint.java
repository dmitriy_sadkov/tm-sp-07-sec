package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.sadkov.tm.dto.TaskDTO;
import ru.sadkov.tm.endpoint.api.ITaskEndpoint;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.service.ITaskService;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.sadkov.tm.endpoint.api.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Nullable
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "taskName") @Nullable final String taskName) {
        @Nullable final Task task = taskService.findTaskByName(taskName);
        if (task == null) return null;
        return taskService.convert(task);
    }

    @WebMethod
    public boolean saveTask(
            @WebParam(name = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description) {
        return taskService.saveTask(taskName, projectName, description);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "taskName") @Nullable final String taskName) {
        taskService.removeTask(taskName);
    }

    @WebMethod
    public void updateTask(
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName,
            @WebParam(name = "description") @Nullable final String description) {
        taskService.update(oldName, newName, description);
    }

    @Nullable
    @WebMethod
    public String startTask(
            @WebParam(name = "taskName") @Nullable final String taskName) {
        return taskService.startTask(taskName);
    }

    @Nullable
    @WebMethod
    public String endTask(
            @WebParam(name = "taskName") @Nullable final String taskName) {
        return taskService.endTask(taskName);
    }

    @NotNull
    @WebMethod
    public List<TaskDTO> findAllTasks() {
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        for (@NotNull final Task task: taskService.findAll()) {
            tasks.add(taskService.convert(task));
        }
        return tasks;
    }

    @WebMethod
    public void clearTasks() {
        taskService.clear();
    }
}
