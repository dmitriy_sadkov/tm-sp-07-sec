package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.endpoint.api.IProjectEndpoint;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.service.IProjectService;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;


@WebService(endpointInterface = "ru.sadkov.tm.endpoint.api.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @WebMethod
    public boolean createProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description) {
        return projectService.persist(userId, projectName, description);
    }

    @WebMethod
    public void updateProject(
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName,
            @WebParam(name = "description") @Nullable final String description) {
        projectService.update(oldName, newName, description);
    }

    @Nullable
    @WebMethod
    public ProjectDTO findOneProjectByName(
            @WebParam(name = "projectName") @Nullable final String projectName) {
        @Nullable final Project project = projectService.findOneByName(projectName);
        if (project == null) return null;
        return projectService
                .convert(project);
    }

    @Nullable
    @WebMethod
    public String startProject(
            @WebParam(name = "projectName") @Nullable final String projectName) {
        return projectService.startProject(projectName);
    }

    @WebMethod
    public @Nullable String endProject(
            @WebParam(name = "projectName") @Nullable final String projectName) {
        return projectService.endProject(projectName);
    }

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects() {
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        System.out.println(projectService);
        for (@NotNull final Project project : projectService.findAll()) {
            projects.add(projectService.convert(project));
        }
        return projects;
    }


    @WebMethod
    public void clearProjects() {
        projectService.clear();
    }

}
