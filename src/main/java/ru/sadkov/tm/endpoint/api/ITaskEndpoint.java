package ru.sadkov.tm.endpoint.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @Nullable
    @WebMethod
    TaskDTO findTaskByName(
            @WebParam(name = "taskName") @Nullable final String taskName);

    @WebMethod
    boolean saveTask(
            @WebParam(name = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description);

    @WebMethod
    void removeTask(
            @WebParam(name = "taskName") @Nullable final String taskName);

    @WebMethod
    void updateTask(
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName,
            @WebParam(name = "description") @Nullable final String description);

    @Nullable
    @WebMethod
    String startTask(
            @WebParam(name = "taskName") @Nullable final String taskName);

    @Nullable
    @WebMethod
    String endTask(
            @WebParam(name = "taskName") @Nullable final String taskName);

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasks();

    @WebMethod
    void clearTasks();
}
