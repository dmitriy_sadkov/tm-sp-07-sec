package ru.sadkov.tm.endpoint.api;

import com.ibm.wsdl.extensions.soap.SOAPConstants;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    boolean createProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description);

    @WebMethod
    void updateProject(
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName,
            @WebParam(name = "description") @Nullable final String description);

    @Nullable
    @WebMethod
    ProjectDTO findOneProjectByName(
            @WebParam(name = "projectName") @Nullable final String projectName);

    @Nullable
    @WebMethod
    String startProject(
            @WebParam(name = "projectName") @Nullable final String projectName);

    @WebMethod
    @Nullable String endProject(
            @WebParam(name = "projectName") @Nullable final String projectName);

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjects();


    @WebMethod
    void clearProjects();
}
