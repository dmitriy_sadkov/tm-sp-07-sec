package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;
import ru.sadkov.tm.service.IUserService;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProjectController {

    @NotNull
    @Autowired
    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;

    @NotNull
    @Autowired
    IUserService userService;

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public List<ProjectDTO> projects(){
        @NotNull final List<Project> projectList = projectService.findAll();
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        for (@NotNull final Project p:projectList) {
            projects.add(projectService.convert(p));
        }
        return projects;
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.GET)
    public ProjectDTO getProjects(@PathVariable(name = "id") @NotNull final String id){
        return projectService.convert(projectService.findOne(id));
    }

    @RequestMapping(value = "/projects", method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE)
    public void createProject(@RequestBody final ProjectDTO projectDTO){
        projectService.saveProjectFromDTO(projectDTO);
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.PUT)
    public void updateProject(@RequestBody final ProjectDTO projectDTO){
        projectService.update(projectDTO.getId(),projectDTO.getName(),projectDTO.getDescription());
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.DELETE)
    public void deleteProject(@PathVariable(name = "id") @NotNull final String id){
        projectService.remove(id);
    }

}
