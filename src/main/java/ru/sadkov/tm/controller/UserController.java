package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sadkov.tm.model.JwtRequest;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.User;
import ru.sadkov.tm.model.UserDTO;
import ru.sadkov.tm.service.IUserService;
import ru.sadkov.tm.util.JWTTokenUtil;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private IUserService userService;

//    @GetMapping("/register")
//    public String registrationForm(Model model) {
//        return "registration";
//    }

//    @PostMapping("/registration")
//    public String registration(Model model, @ModelAttribute("username") String login, @ModelAttribute("password") String password) {
//        @NotNull final User user = userService.createUser(login, password);
//        model.addAttribute("user", user);
//        userService.userRegister(user);
//        return "redirect:/";
//    }
//
//    @GetMapping("/profile/{login}")
//    public String profile(Model model, @PathVariable("login") String login) {
//        @Nullable final User user = userService.findOneByLogin(login);
//        if (user == null) return "redirect:/";
//        model.addAttribute("user", user);
//        List<Project> projectList = user.getProjectList();
//        model.addAttribute("projects", projectList);
//        return "profile";
//    }

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JWTTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(token);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
        @NotNull final User us = userService.createUser(user.getLogin(),user.getPassword());
        userService.userRegister(us);
        return ResponseEntity.ok(us);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}


